#ifndef __AVS_ALSA_MICROPHONE_WRITER_H
#define __AVS_ALSA_MICROPHONE_WRITER_H

// Qt
#include <QThread>

// AVS
#include <AVSCommon/AVS/AudioInputStream.h>

// Own
#include "IInputDevice.h"

namespace avs {
namespace input {
namespace detail {

class AlsaMicrophoneWriterThread : public QThread
{
	Q_OBJECT

public:
	AlsaMicrophoneWriterThread(
		alexaClientSDK::avsCommon::avs::AudioInputStream& stream,
		input::IInputDevice& microphone,
		QObject* parent = Q_NULLPTR);
	virtual ~AlsaMicrophoneWriterThread() override;

public Q_SLOTS:
	void stop();

private:
	void run() override;

private:
	bool m_running;
	alexaClientSDK::avsCommon::avs::AudioInputStream& m_stream;
	input::IInputDevice& m_microphone;
};

template <typename... Args>
auto makeRawAlsaMicrophoneWriterThread(Args&&... args)
{
	return new AlsaMicrophoneWriterThread(std::forward<Args>(args)...);
}

} // namespace detail
} // namespace input
} // namespace avs

#endif // __AVS_ALSA_MICROPHONE_WRITER_H
