#ifndef __AVS_ALSA_INPUTDEVICE_H
#define __AVS_ALSA_INPUTDEVICE_H

// Alsa
#include <alsa/asoundlib.h>

// Qt
#include <QString>

// Own
#include "IInputDevice.h"

namespace avs {
namespace input {

class AlsaInputDevice : public IInputDevice
{
public:
	AlsaInputDevice(
		const QString &device,
		unsigned int samplerate,
		unsigned int channels,
		snd_pcm_format_t format);

	bool open() override;
	void close() override;
	bool isOpened() override;
	bool read(QByteArray &buffer, size_t numSamples) override;
	size_t bytesPerSample() override;

private:
	// TODO: static?
	void cleanup(snd_pcm_t **pcm, snd_pcm_hw_params_t **hwparams);
	void cleanupPcm(snd_pcm_t **pcm);

private:
	const QString m_device;
	const unsigned int m_samplerate;
	const unsigned int m_channels;
	const snd_pcm_format_t m_format;

	snd_pcm_t *m_deviceHandle;
};

} // namespace input
} // namespace avs

#endif // __AVS_ALSA_INPUTDEVICE_H
