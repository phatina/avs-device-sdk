#ifndef __AVS_ALSA_MICROPHONE_STREAMER_H
#define __AVS_ALSA_MICROPHONE_STREAMER_H

// Qt
#include <QObject>
#include <QPointer>

// AVS
#include <AVSCommon/AVS/AudioInputStream.h>

// Own
#include "IInputDeviceStreamer.h"
#include "AlsaMicrophoneWriter.h"

namespace avs {
namespace input {

class AlsaMicrophoneStreamer : public QObject, public input::IInputDeviceStreamer
{
	Q_OBJECT

public:
	AlsaMicrophoneStreamer(
		std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> stream,
		QObject* parent = Q_NULLPTR);
	virtual ~AlsaMicrophoneStreamer() override;

	bool initialize() override;

public Q_SLOTS:
	bool startStreaming() override;
	bool stopStreaming() override;

private:
	std::shared_ptr<alexaClientSDK::avsCommon::avs::AudioInputStream> m_audioInputStream;
	std::unique_ptr<IInputDevice> m_micInputDevice;

	QPointer<detail::AlsaMicrophoneWriterThread> m_worker;
};

template <typename... Args>
auto makeRawAlsaMicrophoneStreamer(Args&&... args)
{
	return new AlsaMicrophoneStreamer{std::forward<Args>(args)...};
}

} // namespace input
} // namespace avs

#endif // __AVS_ALSA_MICROPHONE_STREAMER_H
