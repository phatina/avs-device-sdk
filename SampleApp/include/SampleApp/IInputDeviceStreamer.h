#ifndef __AVS_INPUT_DEVICE_STREAMER_IFACE_H
#define __AVS_INPUT_DEVICE_STREAMER_IFACE_H

namespace avs {
namespace input {

class IInputDeviceStreamer
{
public:
	virtual ~IInputDeviceStreamer() = default;

	virtual bool initialize() = 0;

	virtual bool startStreaming() = 0;
	virtual bool stopStreaming() = 0;
};

} // namespace input
} // namespace avs

#endif // __AVS_INPUT_DEVICE_STREAMER_IFACE_H
