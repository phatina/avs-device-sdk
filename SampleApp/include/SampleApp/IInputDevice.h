#ifndef __AVS_INPUT_DEVICE_IFACE_H
#define __AVS_INPUT_DEVICE_IFACE_H

// Qt
#include <QByteArray>

namespace avs {
namespace input {

class IInputDevice
{
public:
	virtual ~IInputDevice() = default;

	/**
	 * Open input device, if open success or device is already opened
	 * return true, false otherwise
	 */
	virtual bool open() = 0;

	/*
	 * Close input device
	 */
	virtual void close() = 0;

	/*
	 * Test if device is already opened
	 */
	virtual bool isOpened() = 0;

	/*
	 * Read data from device, function blocks until some data are available
	 * if false is returned, next call of isOpened will return false
	 */
	virtual bool read(QByteArray &buffer, size_t numSamples) = 0;

	/*
	 * Get one sample size in bytes
	 */
	virtual size_t bytesPerSample() = 0;
};

} // namespace input
} // namespace avs

#endif // __AVS_INPUT_DEVICE_IFACE_H
