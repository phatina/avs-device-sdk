#include "SampleApp/AlsaMicrophoneWriter.h"

// Qt
#include <QByteArray>

// TODO: switch to nsdk debug
#include <QDebug>

namespace avs {
namespace input {
namespace detail {

namespace {

// 40 * 2048 =~ 80000 samples = 2.5 secs of 16kHz
constexpr auto INPUT_BUFFER_NUM_SAMPLES{2048};
constexpr auto MIC_AUDIO_BUFFERS_COUNT{40};

} // unnamed namespace

using alexaClientSDK::avsCommon::avs::AudioInputStream;

AlsaMicrophoneWriterThread::AlsaMicrophoneWriterThread(
		AudioInputStream& stream,
		IInputDevice& microphone,
		QObject* parent)
	: QThread{parent}
	, m_running{false}
	, m_stream{stream}
	, m_microphone{microphone}
{
}

AlsaMicrophoneWriterThread::~AlsaMicrophoneWriterThread() {}

void AlsaMicrophoneWriterThread::stop()
{
	m_running = false;
}

void AlsaMicrophoneWriterThread::run()
{
	auto writer{
		m_stream.createWriter(
			AudioInputStream::Writer::Policy::NONBLOCKABLE)};

	if (!writer) {
		qDebug() << "Failed to create an input devive writer";
		return;
	}

	qDebug() << "Launching mic writer";

	m_running = true;

	QByteArray readBuffer;
	while (m_running) {
		if (!m_microphone.read(readBuffer, INPUT_BUFFER_NUM_SAMPLES)) {
			qDebug() << "Failed to read from Alsa device";
			continue;
		}

		writer->write(
			readBuffer.data(),
			readBuffer.size() / 2 /* nWords */);
	}
}

} // namespace detail
} // namespace input
} // namespace avs
