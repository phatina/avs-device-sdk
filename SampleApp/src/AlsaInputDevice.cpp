#include "SampleApp/AlsaInputDevice.h"

// TODO: uncomment
//#include "nsdkDebug.h"

//DBG_IMPL_DEBUG_MODULE(AlexaVoiceService);

#define DBG_ENTER(x)
#define DBG_TRACE(x)
#define DBG_INFO(x)
#define DBG_ERROR(x)
#define DBG_WARN(x)

namespace avs {
namespace input {

namespace {

constexpr size_t INPUT_BUFFER_FRAME_SIZE{2048};

} // unnamed namespace

AlsaInputDevice::AlsaInputDevice(
		const QString &device,
		unsigned int samplerate,
		unsigned int channels,
		snd_pcm_format_t format)
	: m_device{device}
	, m_samplerate{samplerate}
	, m_channels{channels}
	, m_format{format}
	, m_deviceHandle{nullptr}
{}

bool AlsaInputDevice::open()
{
	DBG_ENTER("");
	if (m_deviceHandle)
		return true;

	int err{
		::snd_pcm_open(
			&m_deviceHandle,
			m_device.toUtf8().data(),
			SND_PCM_STREAM_CAPTURE,
			0)};
	if (err < 0) {
		DBG_ERROR("Cannot open capture device '" << m_device << "': " << ::snd_strerror(err));
		return false;
	}

	DBG_TRACE("Capture device opened.");

	snd_pcm_hw_params_t *hw_params{nullptr};

	if ((err = ::snd_pcm_hw_params_malloc(&hw_params)) < 0) {
		DBG_ERROR("Cannot allocate hardware parameter structure: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params allocated");

	if ((err = ::snd_pcm_hw_params_any(m_deviceHandle, hw_params)) < 0) {
		DBG_ERROR("Cannot initialize hardware parameter structure: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params initialized");

	if ((err = ::snd_pcm_hw_params_set_access(m_deviceHandle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
		DBG_ERROR("Cannot set access type: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params access set");

	if ((err = ::snd_pcm_hw_params_set_format(m_deviceHandle, hw_params, m_format)) < 0) {
		DBG_ERROR("Cannot set sample format: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params format set: " << m_format);

	if ((err = ::snd_pcm_hw_params_set_rate(m_deviceHandle, hw_params, m_samplerate, 0)) < 0) {
		DBG_ERROR("Cannot set sample rate: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params rate set: " << m_samplerate);

	if ((err = ::snd_pcm_hw_params_set_channels(m_deviceHandle, hw_params, m_channels)) < 0) {
		DBG_ERROR("Cannot set channel count: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	unsigned int channels{0};
	if ((err = ::snd_pcm_hw_params_get_channels(hw_params, &channels)) < 0) {
		DBG_ERROR("Cannot get channel count: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	if (channels != m_channels) {
		DBG_ERROR("Could not set channel count to: " << m_channels);
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params channels was set to: " << channels);

	if ((err = ::snd_pcm_hw_params(m_deviceHandle, hw_params)) < 0) {
		DBG_ERROR("Cannot set device parameters: " << ::snd_strerror(err));
		cleanup(&m_deviceHandle, &hw_params);
		return false;
	}

	DBG_TRACE("hw_params set");

	::snd_pcm_hw_params_free(hw_params);

	return true;
}

void AlsaInputDevice::cleanup(snd_pcm_t **pcm, snd_pcm_hw_params_t **hwparams)
{
	DBG_ENTER("");
	cleanupPcm(pcm);

	if (*hwparams) {
		::snd_pcm_hw_params_free(*hwparams);
		*hwparams = nullptr;
	}
}

void AlsaInputDevice::cleanupPcm(snd_pcm_t **pcm)
{
	DBG_ENTER("");
	if (*pcm) {
		::snd_pcm_close(*pcm);
		*pcm = nullptr;
	}
}

void AlsaInputDevice::close()
{
	DBG_ENTER("");
	cleanupPcm(&m_deviceHandle);
}

bool AlsaInputDevice::isOpened()
{
	return m_deviceHandle;
}

bool AlsaInputDevice::read(QByteArray &buffer, size_t numSamples)
{
	if (!m_deviceHandle)
		return false;

	if (numSamples == 0)
		numSamples = INPUT_BUFFER_FRAME_SIZE;

	// make sure input buffer is big enough
	buffer.resize(numSamples * bytesPerSample());

	auto framesRead{
		::snd_pcm_readi(
			m_deviceHandle,
			buffer.data(),
			numSamples)};

	while (framesRead == -EPIPE) {
		// audio overrun occured. reinitialize audio again
		DBG_WARN("AlsaInputDevice: audio overrun occurred. Recovering.");
		auto err{
			::snd_pcm_prepare(m_deviceHandle)};
		if (err < 0) {
			DBG_ERROR("AlsaInputDevice: Cannot prepare audio interface for use: " << ::snd_strerror(err));
			cleanupPcm(&m_deviceHandle);
			return false;
		}
	}

	if (framesRead <= 0) {
		DBG_ERROR("AlsaInputDevice: Cannot read data: " << ::snd_strerror(framesRead));
		cleanupPcm(&m_deviceHandle);
		return false;
	}

	buffer.resize(framesRead * bytesPerSample());

	return true;
}

size_t AlsaInputDevice::bytesPerSample()
{
	return snd_pcm_format_width(m_format) / 8;
}

} // namespace input
} // namespace avs
