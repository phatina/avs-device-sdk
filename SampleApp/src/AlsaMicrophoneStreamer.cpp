#include "SampleApp/AlsaMicrophoneStreamer.h"

// STL
#include <memory>

// TODO: switch to nsdk debug
#include <QDebug>

// AVS
#include <AVSCommon/Utils/Memory/Memory.h>

// Own
#include "SampleApp/AlsaInputDevice.h"

namespace avs {
namespace input {

using namespace detail;

using alexaClientSDK::avsCommon::avs::AudioInputStream;

namespace {

// 40 * 2048 =~ 80000 samples = 2.5 secs of 16kHz
constexpr auto MICROPHONE_SAMPLE_RATE{16000};

} // unnamed namespace

AlsaMicrophoneStreamer::AlsaMicrophoneStreamer(
		std::shared_ptr<AudioInputStream> stream,
		QObject* parent)
	: QObject{parent}
	, m_audioInputStream{stream}
	, m_worker{nullptr}
{
}

AlsaMicrophoneStreamer::~AlsaMicrophoneStreamer()
{
	if (m_worker)
		return;

	stopStreaming();
}

bool AlsaMicrophoneStreamer::initialize()
{
	// TODO: thrift NsdkAvsSettingsTalkInputDevice
	auto inputDevice{"default"};

	m_micInputDevice = std::make_unique<AlsaInputDevice>(
		inputDevice,
		MICROPHONE_SAMPLE_RATE,
		1,
		SND_PCM_FORMAT_S16_LE);

	return true;
}

bool AlsaMicrophoneStreamer::startStreaming()
{
	if (m_worker)
		return false;

	if (!m_micInputDevice->open()) {
		qDebug() << "Failed to open microphone";
		return false;
	}

	m_worker = makeRawAlsaMicrophoneWriterThread(
		*m_audioInputStream,
		*m_micInputDevice);
	QObject::connect(
		m_worker,
		&AlsaMicrophoneWriterThread::finished,
		m_worker,
		&QObject::deleteLater);

        qDebug() << "Starting mic streamer";

	m_worker->start();

	return true;
}

bool AlsaMicrophoneStreamer::stopStreaming()
{
    if (!m_worker)
		return false;

	qDebug() << "Stopping mic streamer";

    m_worker->stop();
    m_worker->wait();
    m_worker.clear();

	m_micInputDevice->close();

	return true;
}

} // namespace sampleApp
} // namespace alexaClientSDK
